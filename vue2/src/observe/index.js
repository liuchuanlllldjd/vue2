import { newArrayPrototype } from "./array";
import Dep from './dep'
class Observe {
  constructor(data) {
    this.dep = new Dep()

    Object.defineProperty(data, '__ob__', {
      value: this,
      enumerable: false // 将__ob__ 变成不可枚举 （循环的时候无法获取到 这样在监控数组类型的数据时就不会死循环）
    });
    if (Array.isArray(data)) {
      data.__proto__ = newArrayPrototype //通过重写数组方法来使其增删改差可以监控到
      this.observeArray(data)
    } else {
      this.walk(data);
    }
    console.log(this)
  }
  walk(data) {
    Object.keys(data).forEach(key => defineReactive(data, key, data[key]))
  }
  observeArray(data) {
    data.forEach(i => observe(i))//只监控对象类型 (包括数组)不监控基础类型 因为不会通过索引来修改 arr[888]=111 但是会通过 arr[2].a=111
  }
}


function dependArray(value){
  value.forEach(i=>{
    console.log(i)
    i.__ob__&&i.__ob__.dep.depend()
    if(Array.isArray(i)){
      dependArray(i);
  }
  })
}
export function defineReactive(target, key, value) {
 let childOb= observe(value)
  let dep = new Dep()
  Object.defineProperty(target, key, {
    get() {
      if (Dep.target) {
        dep.depend(); // 让这个属性的收集器记住当前的watcher
        if(childOb){
          childOb.dep.depend()
          if(Array.isArray(value)){
            dependArray(value)
          }
        }
      }
      return value
    },
    set(newValue) {
      if (newValue === value) return
      observe(newValue)
      value = newValue
      dep.notify()
    }
  })
}

export function observe(data) {
  if (typeof data !== 'object' || data === null) return
  if (data.__ob__ instanceof Observe) return data.__ob__;
  return new Observe(data);
}
