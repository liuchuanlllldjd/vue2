

let oldArrayPrototype=Array.prototype

export let newArrayPrototype = Object.create(oldArrayPrototype)


const methods=[ // 找到所有的变异方法
'push',
'pop',
'shift',
'unshift',
'reverse',
'sort',
'splice'
]

methods.forEach(method=>{
  newArrayPrototype[method]=function(...args){
    const result =oldArrayPrototype[method].call(this,...args)
    let inserted
    const ob =this.__ob__
    switch (method) {
      case 'push':
      case 'unshift': // arr.unshift(1,2,3)
          inserted = args;
          break;
      case 'splice':  // arr.splice(0,1,{a:1},{a:1})
          inserted = args.slice(2);
      default:
          break;
  }
  if(inserted) ob.observeArray(inserted)
  ob.dep.notify()
    return result
  }

})
