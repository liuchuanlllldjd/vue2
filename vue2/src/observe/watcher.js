import Dep from "./dep";
let id =0
class Watcher{
  constructor(vm,fn,options){
    this.id = id++;
    this.renderWatcher = options;
    this.getter = fn;
    this.deps=[]
    this.depsId=new Set()
    this.get()
  }
  get(){
    Dep.target=this
    this.getter()
    Dep.target=null
  }
  addDep(dep){
    if(!this.depsId.has(dep.id)){
      this.depsId.add(dep.id)
      this.deps.push(dep)
      dep.addSub(this)
    }
  }
  update(){
    queueWatcher(this)
  }
  run(){
    console.log('run')
    this.get()
  }
}
let callbacks = [];
let waiting = false;
function flushCallbacks() {
    let cbs = callbacks.slice(0);
    waiting = false;;
    callbacks = [];
    cbs.forEach(cb => cb()); // 按照顺序依次执行
}
export function nextTick(cb) { // 先内部还是先用户的？
  callbacks.push(cb); // 维护nextTick中的cakllback方法
  if (!waiting) {
      // timerFunc()
      Promise.resolve().then(flushCallbacks)
      waiting = true
  }
}


let queue=[]
let has = {};
let pending = false; // 防抖

function flushSchedulerQueue(){
  let flushQueue=queue.slice(0)
  queue=[]
  has={}
  pending=false
  flushQueue.forEach(q=>q.run())
}

function queueWatcher(watcher){
    const {id} = watcher
    if(!has[id]){
      queue.push(watcher)
      has[id]=true
      if(!pending){
        nextTick(flushSchedulerQueue)
        pending = true;
      }
    }
}



export default Watcher
