import { observe } from "./observe"

export function initState(vm){
  const options =vm.$options
  if(options.data){
    initData(vm)
  }

}

export function proxy(vm, target, key){
  Object.defineProperty(vm,key,{
    get(){
      return vm[target][key]
    },
    set(newValue){
      vm[target][key] = newValue
    }
  })
}

function initData(vm){
  let data =vm.$options.data
  data = typeof data ==='function'?data.call(vm):data

  vm._data = data; //我将返回的对象放到了_data上

  observe(data)
   // 将vm._data 用vm来代理就可以了
  for (let key in data) {
      proxy(vm, '_data', key);
  }
}
